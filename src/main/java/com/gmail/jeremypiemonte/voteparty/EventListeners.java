package com.gmail.jeremypiemonte.voteparty;

import org.bukkit.entity.Firework;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class EventListeners implements Listener {
    Main plugin;

    EventListeners(Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void EntityDamagebyEntityEvent(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Firework) {
            Firework fw = (Firework) event.getDamager();
            if (fw.hasMetadata("nodamage")) {
                event.setCancelled(true);
            }
        }
    }
}
