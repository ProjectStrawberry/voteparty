package com.gmail.jeremypiemonte.voteparty;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.StringUtil;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static net.md_5.bungee.api.ChatColor.COLOR_CHAR;

public class Main extends JavaPlugin {
    Main plugin;
    FileConfiguration config;

    @Override
    public void onEnable() {
        plugin = this;

        plugin.getDataFolder().mkdirs();
        plugin.saveDefaultConfig();
        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();

        this.config = plugin.getConfig();

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);

        this.getCommand("voteparty").setExecutor(new VoteParty(this));

        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            new VotePartyExpansion(this).register();
        }

        getLogger().info("VoteParty loaded.");
    }
}

class VoteParty implements TabExecutor {
    Main plugin;
    VoteParty(Main plugin) {
        this.plugin = plugin;
        config = plugin.getConfig();
        votePartyGoal =  config.getInt("vote-party-goal");
        voteCampaignGoal = config.getInt("vote-campaign-goal");
        voteCampRewards = config.getStringList("vote-campaign-rewards");
        voteCampReminder = config.getInt("vote-campaign-reminder");
    }
    FileConfiguration config = null;
    boolean voteParty = false;
    int votePartyGoal;
    int totalGoalVotes = 0;
    int voteCampaignGoal;
    int voteCampReminder;
    List<String> voteCampRewards;
    Map<String, Integer> votes = new HashMap<>();


    public void registerVote(String playerName) {
        int totalGoalVotes = totalGoalVotesIncrease();

        if (totalGoalVotes == voteCampReminder) {
            int left = voteCampaignGoal - totalGoalVotes;
            String reminderMsg = "&a[VoteGoal] #793bffWe're only &d" + left + " votes #793bffaway from our vote goal!";
            reminderMsg = ChatColor.translateAlternateColorCodes('&', reminderMsg);
            reminderMsg = translateHexColorCodes("#", "", reminderMsg);
            Bukkit.broadcastMessage(reminderMsg);
        }

        if (totalGoalVotes == voteCampaignGoal) {
            int randRewardInd = (int) (Math.random() * voteCampRewards.size());
            List<String> possibleRewards =  voteCampRewards;
            String reward = possibleRewards.get(randRewardInd);
            String rewardName = reward.substring(0, reward.indexOf(" | "));
            String rewardCmd = reward.substring(reward.indexOf(" | ") + 2, reward.length()).trim();

            String rewardMsg = "&a[VoteGoal] #793bff&lWe#9d3bff&l'v#ba3bff&le m#cb3bff&let#eb3bff&l ou#eb3bff&lr v#cb3bff&lot#ba3bff&le g#9d3bff&loa#793bff&ll! &dEveryone online is recieving &a" + rewardName;
            rewardMsg = ChatColor.translateAlternateColorCodes('&', rewardMsg);
            rewardMsg = translateHexColorCodes("#", "", rewardMsg);
            Bukkit.broadcastMessage(rewardMsg);

            for (Player player : Bukkit.getOnlinePlayers()) {
                String newRewardCmd = rewardCmd.replace("<PLAYER>", player.getName());
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), newRewardCmd);

                if (!isVanished(player) && !player.hasPotionEffect(PotionEffectType.INVISIBILITY) && player.getGameMode().toString() != "SPECTATOR") {
                    spawnFireworks(player.getLocation(), 1);
                }
            }
        }

        if (!voteParty) return;
        if (!votes.containsKey(playerName)) {
            votes.put(playerName, 1);
        } else {
            int oldVotes = votes.get(playerName);
            votes.put(playerName, oldVotes + 1);
        }
        int voteNum = votes.get(playerName);

        if (voteNum == votePartyGoal) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "voucher give voteparty " + playerName);
        }
    }

    private boolean isVanished(Player player) {
        for (MetadataValue meta : player.getMetadata("vanished")) {
            if (meta.asBoolean()) return true;
        }
        return false;
    }

    public int totalGoalVotesIncrease() {
        int totalGoalsVote = config.getInt("vote-campaign-votes");

        if (totalGoalsVote + 1 == voteCampaignGoal) {
            config.set("vote-campaign-votes", 0);
            plugin.saveConfig();
            return voteCampaignGoal;
        } else {
            config.set("vote-campaign-votes", totalGoalsVote + 1);
            plugin.saveConfig();
            return totalGoalsVote + 1;
        }
    }
    public int getTotalGoalVotes() {
        return config.getInt("vote-campaign-votes");
    }

    public String translateHexColorCodes(String startTag, String endTag, String message) {
        final Pattern hexPattern = Pattern.compile(startTag + "([A-Fa-f0-9]{6})" + endTag);
        Matcher matcher = hexPattern.matcher(message);
        StringBuffer buffer = new StringBuffer(message.length() + 4 * 8);
        while (matcher.find()) {
            String group = matcher.group(1);
            matcher.appendReplacement(buffer, COLOR_CHAR + "x"
                    + COLOR_CHAR + group.charAt(0) + COLOR_CHAR + group.charAt(1)
                    + COLOR_CHAR + group.charAt(2) + COLOR_CHAR + group.charAt(3)
                    + COLOR_CHAR + group.charAt(4) + COLOR_CHAR + group.charAt(5)
            );
        }
        return matcher.appendTail(buffer).toString();
    }

    public void spawnFireworks(Location location, int amount) {
        Location loc = location;
        Firework fw = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
        FireworkMeta fwm = fw.getFireworkMeta();

        fwm.setPower(2);
        fwm.addEffect(FireworkEffect.builder().withColor(Color.PURPLE, Color.FUCHSIA, Color.LIME).flicker(true).build());

        fw.setMetadata("nodamage", new FixedMetadataValue(plugin, true));
        fw.setFireworkMeta(fwm);
        fw.detonate();

        for (int i = 0; i < amount; i++) {
            Firework fw2 = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
            fw2.setMetadata("nodamage", new FixedMetadataValue(plugin, true));
            fw2.setFireworkMeta(fwm);
        }
    }

    public void spawnJackpotFireworks(Location location, int amount) {
        Location loc = location;
        Firework fw = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
        FireworkMeta fwm = fw.getFireworkMeta();

        fwm.setPower(2);
        fwm.addEffect(FireworkEffect.builder().withColor(Color.ORANGE, Color.YELLOW, Color.GREEN).flicker(true).build());

        fw.setMetadata("nodamage", new FixedMetadataValue(plugin, true));
        fw.setFireworkMeta(fwm);
        fw.detonate();

        for (int i = 0; i < amount; i++) {
            Firework fw2 = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
            fw2.setMetadata("nodamage", new FixedMetadataValue(plugin, true));
            fw2.setFireworkMeta(fwm);
        }
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length > 0) {
            if ("help".equals(args[0])) {
                sender.sendMessage(ChatColor.GOLD + "/voteparty on - Turns a vote party on.");
                sender.sendMessage(ChatColor.GOLD + "/voteparty off - Turns a vote party off.");
                sender.sendMessage(ChatColor.GOLD + "/voteparty setlimit [integer] - Sets minimum amount of votes to recieve a vote party ticket for the next/current vote party.");
                sender.sendMessage(ChatColor.GOLD + "/voteparty goal - Shows the current vote goal.");
                sender.sendMessage(ChatColor.GOLD + "/voteparty reload - Reloads vote party config settings.");
                return true;
            } else if ("register".equals(args[0])) {
                if (!sender.hasPermission("voteparty.admin")) {
                    sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }
                if (args.length < 2) {
                    sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "Usage: /voteparty register <player>");
                    return true;
                }
                assert args[1] != null;
                registerVote(args[1]);
                if (voteParty) sender.sendMessage(ChatColor.GOLD + args[1] + "'s vote has been registered towards the vote party");
                return true;
             } else if ("on".equals(args[0])) {
                if (!sender.hasPermission("voteparty.admin")) {
                    sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }
                if (voteParty) {
                    sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "A vote party is already ongoing");
                    return true;
                }
                voteParty = true;
                Bukkit.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "A vote party has started! Vote to be invited to pick a prize on the vote wall!");
                return true;
            } else if ("off".equals(args[0])) {
                if (!sender.hasPermission("voteparty.admin")) {
                    sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }
                if (!voteParty) {
                    sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "There is no vote party ongoing");
                    return true;
                }
                voteParty = false;
                votes = new HashMap<>();
                Bukkit.broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + "The vote party has ended. Thanks for voting!");
                return true;
            } else if ("reload".equals(args[0])) {
                if (!sender.hasPermission("voteparty.admin")) {
                    sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }
                plugin.reloadConfig();
                plugin.config = plugin.getConfig();
                config = plugin.getConfig();
                votePartyGoal = config.getInt("vote-party-goal");
                voteCampaignGoal = config.getInt("vote-campaign-goal");
                voteCampRewards = config.getStringList("vote-campaign-rewards");
                voteCampReminder = config.getInt("vote-campaign-reminder");
                sender.sendMessage(ChatColor.GOLD + "VoteParty has been reloaded.");
                return true;
            } else if ("rewards".equals(args[0])) {
                List<String> possibleRewards =  voteCampRewards;
                String[] rewards = new String[possibleRewards.size()];
                for (int i = 0; i < possibleRewards.size(); i++) {
                    String rewardLine = possibleRewards.get(i);
                    rewards[i] = rewardLine.substring(0, rewardLine.indexOf(" | "));
                }
                sender.sendMessage(ChatColor.GOLD + "There are " + ChatColor.GREEN+ possibleRewards.size() + ChatColor.GOLD + " possible rewards to get from hitting vote goal:");
                sender.sendMessage(ChatColor.LIGHT_PURPLE + Arrays.toString(rewards).replace("[", "").replace("]", ""));
                return true;
            } else if ("setlimit".equals(args[0])) {
                if (!sender.hasPermission("voteparty.admin")) {
                    sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }
                if (args.length < 2) {
                    sender.sendMessage(ChatColor.RED + "USAGE: " + ChatColor.DARK_RED + "/voteparty setlimit [integer]");
                    return true;
                }
                int limit = Integer.parseInt(args[1]);
                votePartyGoal = limit;
                sender.sendMessage("The vote goal for the next/current vote party has been set to " + limit + " votes");
                return true;
            } else if ("goal".equals(args[0])) {
                sender.sendMessage(ChatColor.GOLD + "" +  ChatColor.BOLD + "Vote Goal: " + ChatColor.RESET + ChatColor.GOLD + getTotalGoalVotes() + " / " + voteCampaignGoal);
            } else if ("jackpot".equals(args[0])) {
                if (!sender.hasPermission("voteparty.admin")) {
                    sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }
                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.DARK_RED + "You must be a player to use this command.");
                    return true;
                }
                Player player = (Player) sender;
                spawnJackpotFireworks(player.getLocation(), 1);
            }
        }
        return true;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (args.length == 1) {
            if (sender.hasPermission("voteparty.admin")) {
                tabComplete.add("on");
                tabComplete.add("off");
                tabComplete.add("setlimit");
                tabComplete.add("register");
                tabComplete.add("help");
            }
            tabComplete.add("goal");
            tabComplete.add("rewards");
        }

        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }
}



class VotePartyExpansion extends PlaceholderExpansion {

    private Main plugin;

    /**
     * Since we register the expansion inside our own plugin, we
     * can simply use this method here to get an instance of our
     * plugin.
     *
     * @param plugin
     *        The instance of our plugin.
     */
    public VotePartyExpansion(Main plugin) {
        this.plugin = plugin;
    }

    /**
     * Because this is an internal class,
     * you must override this method to let PlaceholderAPI know to not unregister your expansion class when
     * PlaceholderAPI is reloaded
     *
     * @return true to persist through reloads
     */
    @Override
    public boolean persist() {
        return true;
    }

    /**
     * Because this is a internal class, this check is not needed
     * and we can simply return {@code true}
     *
     * @return Always true since it's an internal class.
     */
    @Override
    public boolean canRegister() {
        return true;
    }

    /**
     * The name of the person who created this expansion should go here.
     * <br>For convienience do we return the author from the plugin.yml
     *
     * @return The name of the author as a String.
     */
    @Override
    public String getAuthor() {
        return plugin.getDescription().getAuthors().toString();
    }

    /**
     * The placeholder identifier should go here.
     * <br>This is what tells PlaceholderAPI to call our onRequest
     * method to obtain a value if a placeholder starts with our
     * identifier.
     * <br>The identifier has to be lowercase and can't contain _ or %
     *
     * @return The identifier in {@code %<identifier>_<value>%} as String.
     */
    @Override
    public String getIdentifier() {
        return "voteparty";
    }

    /**
     * This is the version of the expansion.
     * <br>You don't have to use numbers, since it is set as a String.
     *
     * For convienience do we return the version from the plugin.yml
     *
     * @return The version as a String.
     */
    @Override
    public String getVersion() {
        return plugin.getDescription().getVersion();
    }

    /**
     * This is the method called when a placeholder with our identifier
     * is found and needs a value.
     * <br>We specify the value identifier in this method.
     * <br>Since version 2.9.1 can you use OfflinePlayers in your requests.
     *
     * @param  player
     *         A {@link org.bukkit.Player Player}.
     * @param  identifier
     *         A String containing the identifier/value.
     *
     * @return possibly-null String of the requested identifier.
     */
    @Override
    public String onPlaceholderRequest(Player player, String identifier) {

        if (player == null) {
            return "";
        }

        if (identifier.equals("curvotes")) {
            return plugin.config.getString("vote-campaign-votes");
        }

        if (identifier.equals("totalvotes")) {
            return plugin.config.getString("vote-campaign-goal");
        }

        // We return null if an invalid placeholder (f.e. %someplugin_placeholder3%)
        // was provided
        return null;
    }
}